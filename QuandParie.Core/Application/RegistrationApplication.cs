﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {

        private readonly ICustomerRepository client;


        private readonly IDocumentRepository document;


        private readonly IIdentityProofer proofIdentity1;


        private readonly IAddressProofer proofIdentity2;

        public RegistrationApplication(
            ICustomerRepository client, 
            IDocumentRepository document, 
            IIdentityProofer proofIdentity1,
            IAddressProofer proofIdentity2)
        {
            this.client = client;
            this.document = document;
            this.proofIdentity1 = proofIdentity1;
            this.proofIdentity2 = proofIdentity2;
        }

        /// Create the account
        public async Task<IReadOnlyCustomer> CreateAccount(string email, string firstName, string lastName)
        {
            var account = new Customer(email, firstName, lastName);
            await client.SaveAsync(account);

            return account;
        }

        /// Get the account
        public async Task<IReadOnlyCustomer> GetAccount(string account)
        {
            return await client.GetAsync(account);
        }

        /// Upload the identity proof
        public async Task<bool> UploadIdentityProof(string email, byte[] identityProof)
        {
            var client = await this.client.GetAsync(email);
            if (!proofIdentity1.Validates(client, identityProof))
                return false;

            await document.SaveAsync(DocumentType.IdentityProof, email, identityProof);

            client.IsIdentityVerified = true;
            await this.client.SaveAsync(client);

            return true;
        }

        /// Upload the address proof
        public async Task<bool> UploadAddressProof(string email, byte[] adressProof)
        {
            var client = await this.client.GetAsync(email);
            if (!proofIdentity2.Validates(client, out var s2, adressProof))
                return false;

            await document.SaveAsync(DocumentType.AddressProof, email, adressProof);

            client.Address = s2;
            await this.client.SaveAsync(client);

            return true;
        }
    }
}
