﻿using QuandParie.Core.Domain;
using QuandParie.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.Application
{
    public class WageringApplication
    {
        private readonly IWagerRepository wagerRepository;
        private readonly ICustomerRepository customerRepository;
        private readonly IOddsRepository oddsRepository;

        public WageringApplication(
            IWagerRepository wagerRepository,
            ICustomerRepository customerRepository,
            IOddsRepository oddsRepository)
        {
            this.wagerRepository = wagerRepository;
            this.customerRepository = customerRepository;
            this.oddsRepository = oddsRepository;
        }

        public async Task<Wager> PlaceWager(string customerEmail, decimal amount, IReadOnlyList<Guid> oddsIds)
        {
            var customer = await customerRepository.GetAsync(customerEmail)
                ?? throw new ArgumentException($"{customerEmail} is not a valid customer", nameof(customerEmail));

            if (!customer.CanWager)
                throw new InvalidOperationException($"{customerEmail} does not have the right to wager");

            var odds = await GetOdds(oddsIds);

            customer.Debit(amount);

            var wager = new Wager(customer, odds, amount);
            await wagerRepository.SaveAsync(wager);

            return wager;
        }

        private async Task<IReadOnlyList<Odds>> GetOdds(IReadOnlyList<Guid> oddsIds)
        {
            List<Odds> oddsList = new List<Odds>();
            List<Guid> oddsIdWhichCantBeWagered = new List<Guid>();
            for (int i = 0; i < oddsIds.Count; i++)
            {
                var someOdds = await oddsRepository.GetAsync(oddsIds[i]);
                if (someOdds == null)
                {
                    throw new InvalidOperationException($"{oddsIds[i]} is not some valid odds");
                }
                oddsList.Add(someOdds);

                if (!someOdds.CanBeWagered)
                    oddsIdWhichCantBeWagered.Add(someOdds.Id);
            }

            if (oddsIdWhichCantBeWagered.Count > 0)
                throw new InvalidOperationException($"The following odds already have an outcome : {string.Join(", ", oddsIdWhichCantBeWagered)}");
            
            return oddsList;
        }

        public async Task<IReadOnlyList<Wager>> GetWagers(string email)
        {
            return await wagerRepository.GetWagerOfUser(email);
        }
    }
}
