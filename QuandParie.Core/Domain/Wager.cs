﻿using QuandParie.Core.ReadOnlyInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuandParie.Core.Domain
{
    public class Wager : IReadOnlyWager
    {
        public Guid Id { get; } = Guid.NewGuid();

        public Customer Customer { get; }
        IReadOnlyCustomer IReadOnlyWager.Customer => Customer;


        public IReadOnlyList<Odds> Odds { get; }
        IReadOnlyList<IReadOnlyOdds> IReadOnlyWager.Odds => Odds;
        public IEnumerable<Guid> OddsIds => Odds.Select(odds => odds.Id);

        public decimal Amount { get; }

        public bool IsProcessed { get; private set; }

        public decimal PotentialPayoff
            => Amount * Odds.Product(odds => odds.Value);


        public Wager(Customer customer, IReadOnlyList<Odds> odds, decimal amount)
        {
            Customer = customer;
            Odds = odds;
            Amount = amount;
        }

        public bool TryProcess()
        {
            if (IsProcessed)
                return false;

            if (Odds.Any(odds => odds.Outcome == false))
                ProcessFailure();
            else if (Odds.All(odds => odds.Outcome == true))
                ProcessSuccess();

            return IsProcessed;
        }

        private void ProcessFailure()
            => IsProcessed = true;

        private void ProcessSuccess()
        {
            Customer.Credit(PotentialPayoff);
            IsProcessed = true;
        }
    }
}