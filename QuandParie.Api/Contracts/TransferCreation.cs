﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuandParie.Api.Contracts
{
    public record TransferCreation(
        [Required] string Iban,
        [Required][Range(1, int.MaxValue)] decimal Amount);
}
